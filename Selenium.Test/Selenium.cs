﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Turp.SeleniumTest
{
    public class Selenium
    {
        public void TestQuoteBox()
        {

            try
            {
                Console.Write("Start Date: ");
                var startDate = Console.ReadLine();

                Console.Write("End Date: ");
                var endDate = Console.ReadLine();

                Console.Write("State Abbv: ");
                var state = Console.ReadLine();

                Console.Write("Primary Insured Age: Abbv: ");
                var age = Console.ReadLine();

                Console.Write("Primary Trip Cost: ");
                var cost = Console.ReadLine();


                IWebDriver driver = new ChromeDriver {
                    Url = "http://www.sevencorners.com"
                };
            
                // Maximize Window
                driver.Manage().Window.Maximize();

                // Click on Insurance Plans
                var insurancePlansLink = driver.FindElement(By.XPath(@"//*[@id=""navbar""]/ul/li[1]/a"));
                insurancePlansLink.Click();

                // Click Roundtrip Economy
                var roundtripEconomy = driver.FindElement(By.XPath(@"//*[@id=""navbar""]/ul/li[1]/ul/li/div/div/ul[3]/li[3]/a"));
                roundtripEconomy.Click();
            
                var txtStartDate = driver.FindElement(By.XPath(@"//*[@id=""prelimFieldsPlaceholder_C011""]/div/div/div/div/form-group[1]/div/div/div/div/ui-date/div/input"));
                txtStartDate.SendKeys(startDate);

                var txtEndDate = driver.FindElement(By.XPath(@"//*[@id=""prelimFieldsPlaceholder_C011""]/div/div/div/div/form-group[2]/div/div/div/div/ui-date/div/input"));
                txtEndDate.SendKeys(endDate);

                // Select State
                var stateXPath = @"//*[@id=""ResidenceState""]";
                var select = new SelectElement(driver.FindElement(By.XPath(stateXPath)));
                select.SelectByValue(state);

                var txtAge = driver.FindElement(By.XPath(@"//*[@id=""Age_BND""]"));
                txtAge.SendKeys(age);

                var txtTripCost = driver.FindElement(By.XPath(@"//*[@id=""TrpCos_BND""]"));
                txtTripCost.SendKeys(cost);

                var nextButton = driver.FindElement(By.XPath(@"//*[@id=""prelimFieldsPlaceholder_C011""]/div/div/div/div/p/button"));
                nextButton.Click();
            }
            catch (WebDriverException wdEx)
            {
                Console.WriteLine(wdEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}